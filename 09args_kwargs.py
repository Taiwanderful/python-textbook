# def my_method(arg1, arg2):
#     return arg1 + arg2

# my_method(5, 6)

# def my_long_method(arg1, arg2, arg3, arg4, arg5):
#     return arg1 + arg2 + arg3 + arg4 + arg5

# my_long_method(12, 24, 36 ,48, 60)

# def additional_simplified(*args):
#     return sum(args)

# additional_simplified(1, 3, 5, 7, 1 ,2, 5, 9)

# [*[1,2,3],*[4,5,6]]
def what_are_kwargs(*args, **kwargs):
    print(args)
    print(kwargs)

what_are_kwargs(12, 34, 56)
what_are_kwargs(12, 34, 56, name='Benny', location='Taiwan')

# class Student:
#     def __init__(self, name, school):
#         self.name = name
#         self.school = school
#         self.marks = []

#     def average(self):
#         return sum(self.marks) / len(self.marks)

#     @classmethod
#     def friend(cls, origin, firend_name, *args, **kwargs):
#         return cls(firend_name, origin.school, *args, **kwargs)

# class WorkingStudent(Student):
#     def __init__(self, name, school, salary, job_title):
#         super().__init__(name, school)
#         self.salary = salary
#         self.job_title = job_title

# benny = WorkingStudent('Benny', 'NCCU', 150, 'Software Developer')
# friend = benny.friend(benny, 'Tina', salary=200, job_title='Software Developer')
# friend = benny.friend(benny, 'Tina', 200, job_title='Software Developer')
# friend = benny.friend(benny, 'Tina', salary=200, 'Software Developer')

# print(friend.name)
# print(friend.school)
# print(friend.salary)
# print(friend.job_title)