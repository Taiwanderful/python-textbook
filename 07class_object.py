# lottery_player = {
#     'name': 'Tim',
#     'numbers': (13, 45, 66, 11, 25)
# }

# class LotteryPlayer:
#     def __init__(self):
#         self.name = 'Ivy'
#         self.numbers = (1, 3, 5, 7, 9)

#     def total(self):
#         return sum(self.numbers)

# player1 = LotteryPlayer()
# print(player1.name)
# print(player1.numbers)
# print(player1.total())

# player2 = LotteryPlayer()

# print(player1 == player2)

# class LotteryPlayer:
#     def __init__(self, name):
#         self.name = name
#         self.numbers = (1, 3, 5, 7, 9)

#     def total(self):
#         return sum(self.numbers)

# player1 = LotteryPlayer('John')
# player1.numbers = (1, 2, 3, 4, 5)

# player2 = LotteryPlayer('Ken')

# print(player1.numbers == player2.numbers)

# class Student:
#     def __init__(self, name, school):
#         self.name = name
#         self.school = school
#         self.marks = []

#     def average(self):
#         return sum(self.marks) / len(self.marks)

#     @staticmethod
#     def go_to_school():
#         print("I'm going to school.")

# benny = Student('Benny', 'NCCU')
# benny.marks.append(60)
# benny.marks.append(70)
# print(benny.average())

# Student.go_to_school()