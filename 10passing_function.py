def my_method(another):
    return another()

def add_two_numbers():
    return 35 + 77

# print(my_method(add_two_numbers))
# print(my_method(lambda: 35 + 77))

my_list = [13, 56 ,77 ,484]
print(list(filter(lambda x:x != 13, my_list)))

def not_thirteen(x):
    return x != 13

print(list(filter(not_thirteen, my_list)))
print([x for x in my_list if x != 13])

# practice
def add(x):
    def add2(y):
        return x + y
    return add2

print(add(2)(3))
