# class Student:
#     def __init__(self, name, school):
#         self.name = name
#         self.school = school
#         self.marks = []

#     def average(self):
#         return sum(self.marks) / len(self.marks)

#     def friend(self, firend_name):
#         return Student(firend_name, self.school)

# class WorkingStudent(Student):
#     def __init__(self, name, school, salary):
#         super().__init__(name, school)
#         self.salary = salary

# benny = WorkingStudent('Benny', 'NCCU', 150)
# friend = benny.friend('Tina')
# print(benny.salary)

# print(friend.name)
# print(friend.school)
# print(friend.salary)

class Student:
    def __init__(self, name, school):
        self.name = name
        self.school = school
        self.marks = []

    def average(self):
        return sum(self.marks) / len(self.marks)

    @classmethod
    def friend(cls, origin, firend_name, salary):
        return cls(firend_name, origin.school, salary)

class WorkingStudent(Student):
    def __init__(self, name, school, salary):
        super().__init__(name, school)
        self.salary = salary

benny = WorkingStudent('Benny', 'NCCU', 150)
friend = benny.friend(benny, 'Tina', 200)
print(benny.salary)

print(friend.name)
print(friend.school)
print(friend.salary)