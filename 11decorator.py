from functools import wraps

def my_decorator(func):
    @wraps(func)
    def wrap_func():
        print('In the decorator!')
        func()
        print('After the decorator!')
    return wrap_func

@my_decorator
def my_function():
    print("I'm the function!")

# my_function()

def decorator_with_argument(number):
    def decorator(func):
        @wraps(func)
        def function_that_run_func(*args, **kwargs):
            print('In the decorator!')
            if number == 56:
                print('Not running the function!')
            else:
                print(func(*args, **kwargs) + number)
            print('After the decorator!')
        return function_that_run_func
    return decorator

@decorator_with_argument(5)
def my_function_two(x, y):
    print('Hello')
    return x * y

my_function_two(2, 3)

def add(z):
    def decorator(func):
        @wraps(func)
        def wrap_func(*args):
            return func(*args) + z
        return wrap_func
    return decorator

@add(4)
def multify(x, y):
    return x * y

print(multify(2, 3))